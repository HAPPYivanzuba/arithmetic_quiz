#include <iostream>
#include <cstdlib>
using namespace std;

void show_num(int num);

int main()
{
    int action;
    int l_num;
    int r_num;
    int result;
    int correct_result;
    setlocale(LC_ALL,"Rus");
    srand(time(NULL));
    for(int e=0;e<10;e++)
    {
        action=rand()%3;
        l_num=rand()%100;
        r_num=rand()%100;
        if(action==0)//+
        {
            show_num(l_num);
            cout<<"+";
            show_num(r_num);
            cout <<"=?"<<endl;
            correct_result=l_num+r_num;
        }
        if(action==1)//-
        {
            cout<<l_num<<"-"<<r_num<<"=?"<<endl;
            correct_result=l_num-r_num;
        }
        if(action==2)//*
        {
            do
            {
                l_num=rand()%100;
                r_num=rand()%100;
                correct_result=l_num*r_num;
            }
            while(correct_result>300);
            cout<<l_num<<"*"<<r_num<<"=?"<<endl;
        }
        if(action==3)
        {
            correct_result=rand()%10;
            r_num=r_num+1;
            l_num=correct_result*r_num;
            cout<<l_num<<"/"<<r_num<<"=?"<<endl;
        }
        cin >> result;
        if(result == correct_result)    cout<<"да"<<endl;
            else  cout<<"нет, на самом деле "<<correct_result<<endl;
    }
    return 0;
}


void print_roman(int num)
{
    if (num<0) cout << "-";
    while (num>=1000)
    {
        cout << "m";
        num-=1000;
    }

    while (num>=500)
    {
        cout << "d";
        num-=500;
    }

    while (num>=100)
    {
        cout << "c";
        num-=100;
    }

    while (num>=50)
    {
        cout << "l";
        num-=50;
    }

    while (num>=10)
    {
        cout << "x";
        num-=10;
    }

    while(num>=5)
    {
        cout<<"v";
        num-=5;
    }

    while (num>=1)
    {
        cout<<"i";
        num--;
    }
}

void print_triple(int num, unsigned int order)
{
	//123`456`789, 3 -> сто двадцать три
	//123`456`789, 2 -> четыреста пятьдесят шесть
	//123`456`789, 1 -> семьсот восемьдесят девять

	//4 1000000000000 1000000000
	//3 1000000000 1000000
	//2 1000000 1000
	//1 1000 1
	setlocale(LC_ALL, "rus");
	int Xxx = ((int)num % (int)pow(1000, order)) / pow(1000, order - 1);
	cout << Xxx << endl;

	//трёхзначное
	int part10 = Xxx;
	int part001;
	int part1019;
	int prosto2;
	Xxx = Xxx / 100;//5
	part10 = (part10 - Xxx * 100);//18
	part1019 = part10;
	part001 = part10 % 10;
	part10 = part10 / 10;
	cout << Xxx << " " << part10 << " " << part001 << endl;


	switch (Xxx)
	{
	case 0: break;
	case 1:
		cout << "сто ";
		break;
	case 2:
		cout << "двести ";
		break;
	case 3:
		cout << "триста ";
		break;
	case 4:
		cout << "четыреста ";
		break;
	case 5:
		cout << "пятьсот ";
		break;
	case 6:
		cout << "шестьсот ";
		break;
	case 7:
		cout << "семьсот ";
		break;
	case 8:
		cout << "восемьсот ";
		break;
	case 9:
		cout << "девятьсот ";
		break;
	default:
		cout << "дофигиста ";
	}
	if (part10 * 10 < 20 && part10>0)
	{
		switch (part1019)
		{
		case 0:
			cout << "";
			break;
		case 10:
			cout << "десять ";
			break;
		case 11:
			cout << "одиннадцать ";
			break;
		case 12:
			cout << "двенадцать ";
			break;
		case 13:
			cout << "тринадцать ";
			break;
		case 14:
			cout << "четырнадцать ";
			break;
		case 15:
			cout << "пятнадцать ";
			break;
		case 16:
			cout << "шестнадцать ";
			break;
		case 17:
			cout << "семнадцать ";
			break;
		case 18:
			cout << "восемнадцать ";
			break;
		case 19:
			cout << "девятнадцать ";
			break;
		default:
			cout << "ефигица";
		}
	}

	else
	{

		switch (part10)
		{
		case 0:
			cout << "";
			break;
		case 2:
			cout << "двадцать ";
			break;
		case 3:
			cout << "тридцать ";
			break;
		case 4:
			cout << "сорок ";
			break;
		case 5:
			cout << "пятьдесят ";
			break;
		case 6:
			cout << "шестьдесят ";
			break;
		case 7:
			cout << "семьдесят ";
			break;
		case 8:
			cout << "восемьдесят ";
			break;
		case 9:
			cout << "девяносто ";
			break;
		default:
			cout << "дофигидцать ";
		}

		switch (part001)
		{
		case 0:
			cout << "";
			break;
		case 1:
			cout << "один ";
			break;
		case 2:
			cout << "два ";
			break;
		case 3:
			cout << "три ";
			break;
		case 4:
			cout << "четыре ";
			break;
		case 5:
			cout << "пять ";
			break;
		case 6:
			cout << "шесть ";
			break;
		case 7:
			cout << "семь ";
			break;
		case 8:
			cout << "восемь ";
			break;
		case 9:
			cout << "девять ";
			break;
		}
	}
	cout << endl;
}

void print_text(int num)
{
if (num<0) cout << "минус ";
    cout << (num%1000000)/1000 << " тысяч "<<endl;
    cout << (num%1000) << endl;
}

void show_num(int num)
{
    int method=rand()%3;
    if(method==1) print_roman(num);
    else if(method==2) print_text(num);
    else cout<<num;
    //random:   8 -> 8
    //          8 -> VIII
    //          8 -> восемь

}
